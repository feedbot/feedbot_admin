import re
import consul

from flask import Flask, request, jsonify, flash

import flask_admin as admin
from flask_admin import expose
from flask_mongoengine import MongoEngine
from flask_admin.form import rules
from flask_admin.contrib.mongoengine import ModelView, filters

from feed_bot.feed_bot_worker import sync_tasks as worker_tasks
from feed_bot.storage.models import Channel, User, Worker

# Create application
app = Flask(__name__)

# c = consul.Consul('192.168.43.159')
# mongo_host = c.catalog.service('mongo-primary')[1][0]['ServiceAddress']
#

# Create dummy secrey key so we can use sessions
app.config['SECRET_KEY'] = '123456790'
app.config['MONGODB_SETTINGS'] = {
    'db': 'feed_bot',
    # 'host': 'mongo-primary.service.dc1.consul',
    'host': 'localhost',
    'port': 27017}

# Create models
db = MongoEngine()
db.init_app(app)


################################################################################
# Custom filters for TelegramChannelView

class FilterByTgStatus(filters.BaseMongoEngineFilter):
    def apply(self, query, value, ):
        flt = {'status': value}
        return query.filter(**flt)

    def operation(self):
        return 'status'


# Customized admin views
class UserView(ModelView):
    column_filters = ['username']
    column_searchable_list = ('username', 'full_name')
    can_create = False


class WorkerView(ModelView):
    column_filters = ['name']
    column_searchable_list = ('name', 'phone')

    create_template = 'adminis/worker/create.html'
    edit_template = 'adminis/worker/edit.html'


    def after_model_change(self, form, model, is_created):
        if is_created:
            status, msg = worker_tasks.subscribe_to_bot(model.phone)

            if status:
                flash(msg, 'success')
            else:
                flash(msg, 'error')

    @expose('/request_tg_code/', methods=('POST',), )
    def request_code(self):
        phone = request.json.get('phone')

        pattern = re.compile("\++?\d{12,}$")
        if not pattern.match(phone):
            return jsonify({'status': 'error', 'msg': 'Invalid phone format. (Ex.: +380631234567)'})

        status, msg = worker_tasks.request_code(phone)

        if status:
            return jsonify({'status': 'success', 'msg': msg})
        else:
            return jsonify({'status': 'error', 'msg': msg})

    @expose('/verify_telegram/', methods=('POST',))
    def verify_telegram(self):
        phone = request.json.get('phone')
        sms_code = request.json.get('sms_code')

        pattern = re.compile("\++?\d{12,}$")
        if not pattern.match(phone):
            return jsonify({'status': 'error', 'msg': 'Invalid phone format. (Ex.: +380631234567)'})

        if not phone or not sms_code:
            return jsonify({'status': 'error', 'msg': 'Provide valid phone number and code.'})

        status, tg_id, msg = worker_tasks.authorize_user(phone, sms_code)

        if status:
            return jsonify({'status': 'success', 'msg': msg, 'tg_id': tg_id})
        else:
            return jsonify({'status': 'error', 'msg': msg, 'tg_id': tg_id})


class ChannelView(ModelView):
    column_list = ('name', 'username', 'type', 'worker', 'status')
    column_searchable_list = ('username', 'join_hash')

    column_filters = (
        'username',
        'worker',
        'is_private',
        FilterByTgStatus(Channel.status, 'status', options=(('success', 'success'),
                                                            ('error', 'error'),
                                                            ('pending', 'pending'),
                                                            ('ban', 'ban')))
    )

    form_excluded_columns = ['tg_id', 'ban_history']

    form_widget_args = {
        'status': {
            'readonly': True
        },
        'log': {
            'readonly': True
        },
        'profile': {
            'readonly': True
        }
    }


# Flask views
@app.route('/')
def index():
    return '<a href="/admin/">Click me to get to Admin!</a>'


# Create admin
admin = admin.Admin(app, 'feedbot_admin', template_mode='bootstrap3')

# Add views

admin.add_view(UserView(User, name='Users'))
admin.add_view(WorkerView(Worker, name='Workers'))
admin.add_view(ChannelView(Channel, name='Channels'))


if __name__ == '__main__':
    # Start app
    # app.run(debug=True)
    app.run(host='0.0.0.0', debug=False)
 