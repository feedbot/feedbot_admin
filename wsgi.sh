#!/bin/bash
ps -ef | grep 'uwsgi' | grep -v grep | awk '{print $2}' | xargs -r kill -9
uwsgi --http 0.0.0.0:7777 --wsgi-file admin.py --callable app --processes 4 --threads 2 --buffer-size 65536 --logto server.log & disown